<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/', [ProsesController::class, 'index']);
Route::post('/proses', [ProsesController::class, 'proses']);
Route::post('/npm', [ProsesController::class, 'npm']);
Route::post('/nama', [ProsesController::class, 'nama']);
Route::post('/prodi', [ProsesController::class, 'prodi']);
Route::post('/no_hp', [ProsesController::class, 'nohp']);
Route::post('/ttl', [ProsesController::class, 'ttl']);
Route::post('/jenis_kelamin', [ProsesController::class, 'jenis_kelamin']);
Route::post('/agama', [ProsesController::class, 'agama']);