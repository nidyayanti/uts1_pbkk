<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProsesController extends Controllers
{
    public function index(){
        return view('create');
    }
    public function proses(Request $request){
        $data = array();
        $data['proses'] = $request->proses;
        $data['npm'] = $request->npm;
        $data['nama'] = $request->nama;
        $data['prodi'] = $request->prodi;
        $data['nohp'] = $request->nohp;
        $data['ttl'] = $request->ttl;
        $data['jenis_kelamin'] = $request->jenis_kelamin;
        $data['agama'] = $request -> agama;
   
        return view('view', ['data' => $data]);
    }
}